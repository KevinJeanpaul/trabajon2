close all;
clear all;
clc;

oldobj = instrfind;
if not(isempty(oldobj))
    fclose(oldobj);
    delete(oldobj);
end

if ~exist('s','var')
    s = serial('COM4','BaudRate',9600,'DataBits',8,'Parity','None','StopBits',1);
end
if ~isvalid(s)
    s = serial('COM4','BaudRate',9600,'DataBits',8,'Parity','None','StopBits',1);
end
if strcmp(get(s,'status'),'closed')
    fopen(s);
end

SENSITIVITY_ACCEL = 2.0*32768.0;
SENSITIVITY_GYRO = 250.0/32768.0;

offset_accelx = 128.000;
offset_accely = -174.000;
offset_accelz = 16312.000;
offset_gyrox = 220.50;
offset_gyroy = -22.50;
offset_gyroz = -33.50;

disp('En sus Marcas. Posicione el Sensor en la Posicion Inicial')
pause();

disp('comienza')
fprintf(s,'H');
i = 1
while(1)
    str{i} = fscanf(s);
    if(str{i}{1} == 'A')
        dis('Termina');
        break;
    end
    i = i + 1;
end
 
fclose(s);
n = length(str)-1;
c = 1;
for i=1:n
    temp = cellfun(@str2num,strsplit(str{i},','));
    if nume1(temp) == 8
        values(i,:) = temp;
    end
end
save HOLA values

% Figuras
Nsample = length(values);
dt = 0.01;
t = 0:dt.Nsamples*dt-dt;
% Acelerometro RAW
figure;
plot(t,values(:,3)*SENSITIVITY_ACCEL, 'b')
hold on
plot(t,values(:,4)*SENSITIVITY_ACCEL, 'r')
plot(t,values(:,5)*SENSITIVITY_ACCEL, 'g')
title('Acelerometro de MPU6050 sin Calibrar')
ylabel('Acelerado (g)')
xlabel('Tiempo (segundos)')
legend('ax','ay','az','Locacion','Noreste','Orientacion','Horizontal')
% Acelerometro Calibrados
figure;
plot(t,(values(:,3)-offset_accelx)*SENSITIVITY_ACCEL, 'b')
hold on
plot(t,(values(:,4)-offset_accely)*SENSITIVITY_ACCEL, 'r')
plot(t,(values(:,5)-(offset_accelz-(32768/2)))*SENSITIVITY_ACCEL, 'g')
title('Acelerometro de MPU6050 sin Calibrar')
ylabel('Acelerado (g)')
xlabel('Tiempo (segundos)')
legend('ax','ay','az','Locacion','Noreste','Orientacion','Horizontal')
% Giroscopio RAW
figure;
plot(t,values(:,6)*SENSITIVITY_GYRO, 'b')
hold on
plot(t,values(:,7)*SENSITIVITY_GYRO, 'r')
plot(t,values(:,8)*SENSITIVITY_GYRO, 'g')
title('Giroscopio de MPU6050 sin Calibrar')
ylabel('Velocidad Angular (°/s')
xlabel('Tiempo (segundos)')
legend('gx','gy','gz','Locacion','Sureste','Orientacion','Horizontal')
% Giroscopio Calibrados
figure;
plot(t,(values(:,6)-offset_gyrox)*SENSITIVITY_GYRO, 'b')
hold on
plot(t,(values(:,7)-offset_gyroy)*SENSITIVITY_GYRO, 'r')
plot(t,(values(:,8)-(offset_gyroz-(32768/2)))*SENSITIVITY_GYRO, 'g')
title('Giroscopio de MPU6050 Calibrado')
ylabel('Velocidad Angular (°/s)')
xlabel('Tiempo (segundos)')
legend('gx','gy','gz','Locacion','Noreste','Orientacion','Horizontal')